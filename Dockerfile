# Stage 1: Build the Go app
FROM golang:latest AS builder

WORKDIR /app

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o domain-bot

# run production in a minimal image with only the binary
FROM alpine:latest

COPY --from=builder /app/domain-bot /domain-bot

ENTRYPOINT ["/domain-bot"]
