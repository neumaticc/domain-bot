# domain-bot

A bot made for the [nest.rip](https://nest.rip) [Discord community](https://discord.gg/screenshots) to automatically provide data about domains sent in #domain-suggestions


## To run:

You need [git](https://git-scm.com/) and [docker](https://docs.docker.com/engine/install) installed

```bash
# This is untested on Windows machines!
git clone git@gitlab.com:neumaticc/domain-bot
cd domain-bot

docker compose up -d
```
