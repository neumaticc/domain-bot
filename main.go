package main

import (
	util "domain-bot/rod"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/bwmarrin/discordgo"
	_ "github.com/joho/godotenv/autoload"
)

var (
	rl      = util.NewRateLimiter() // ratelimiter instance
	recents []string                // recent searches
)

func main() {
	dg, err := discordgo.New(os.Getenv("DISCORD_TOKEN"))
	if err != nil {
		fmt.Println("error creating Discord session,", err)
		return
	}

	// init handlers
	dg.AddHandler(ready)
	dg.AddHandler(messageCreate)

	// only want GuildMessage
	dg.Identify.Intents = discordgo.IntentsMessageContent | discordgo.IntentsGuildMessages

	err = dg.Open()
	if err != nil {
		fmt.Println("error opening connection,", err)
		return
	}

	// wait until SIGTERM (ctrl-c)
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	<-sc

	dg.Close()
}

func ready(s *discordgo.Session, event *discordgo.Ready) {
	fmt.Println(fmt.Sprintf("Logged in as %s#%s", s.State.User.Username, s.State.User.Discriminator))

	// set status
	// note to readers: i have NO IDEA why s.UpdateStreamingStatus or any other activity type does not work
	// prob only works as an RPC app connecting to the DC client
	s.UpdateStatusComplex(discordgo.UpdateStatusData{
		Status: "online",
	})
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the authenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.ChannelID != os.Getenv("DISCORD_CHANNEL") {
		return
	}

	foundDomains := strings.Split(m.Content, " ")
	for _, domain := range foundDomains {
		// allow only hostnames, no links
		if strings.Contains(domain, ".") && !strings.Contains(domain, "/") {
			fmt.Println("dv", util.DomainValid(domain))
			if util.DomainValid(domain) {
				for _, d := range recents {
					if d == domain {
						return
					} else {
						recents = append(recents, domain)
					}
				}

				fmt.Println(domain)

				domain = strings.ToLower(domain)
				// rate-limiting
				allowed := rl.Request()
				if !allowed {
					fmt.Printf("%s [%s]: Rate-limited\n", util.ConvertUser(m.Author), domain)
					break
				}

				// whois -- check if registered already

				if isRegistered, err := util.Whois(domain); err != nil {
					fmt.Printf("%s [%s]: WHOIS error\n", util.ConvertUser(m.Author), domain)
				} else if isRegistered {
					fmt.Printf("%s [%s]: Already registered\n", util.ConvertUser(m.Author), domain)

					s.ChannelMessageSendReply(
						m.ChannelID,
						fmt.Sprintf(":no_entry_sign: It appears that this domain is [already registered](<https://who.is/whois/%s>).", domain),
						&discordgo.MessageReference{
							MessageID: m.ID,
							ChannelID: m.ChannelID,
							GuildID:   m.GuildID,
						},
					)
					break
				}

				// run rod logic
				err, res := util.DoRod(domain)
				if err != nil {
					panic(err)
				}

				// in some cases, e,g .CO, tld-list returns something else (e,g: .COM), so protect against that
				// needs to be fixed in the future
				if res.Domain != domain {
					fmt.Printf("%s [%s]: Domain does not match found (%s)\n", util.ConvertUser(m.Author), domain, res.Domain)
					return
				}

				fmt.Printf("%s [%s]: Found: starting at %s from %s (ss: %s)\n", util.ConvertUser(m.Author), domain, res.Price, res.Registrar, res.Image)
				s.ChannelMessageSendReply(
					m.ChannelID,
					fmt.Sprintf("This domain starts at **%s** from **%s**: %s ([tld-list.com](<https://tld-list.com>))", res.Price, res.Registrar, res.Image),
					&discordgo.MessageReference{
						MessageID: m.ID,
						ChannelID: m.ChannelID,
						GuildID:   m.GuildID,
					},
				)

				break
			}
		}
	}
}
