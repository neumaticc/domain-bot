package util

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
)

func ConvertUser(u *discordgo.User) string {
	if u.Discriminator == "0" {
		return fmt.Sprintf("@%s", u.Username)
	} else {
		return fmt.Sprintf("%s#%s", u.Username, u.Discriminator)
	}
}
