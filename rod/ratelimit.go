package util

import (
	"time"
)

type RateLimiter struct {
	ticker *time.Ticker
	allow  bool
}

// NewRateLimiter returns a RateLimiter instance
func NewRateLimiter() *RateLimiter {
	return &RateLimiter{
		ticker: time.NewTicker(30 * time.Second),
		allow:  true,
	}
}

// Request is called whenever a request to be checked is made; it increments the ticker by 1 and returns allowed T|F
func (rl *RateLimiter) Request() bool {
	if rl.allow {
		rl.allow = false
		go func() {
			<-rl.ticker.C
			rl.allow = true
		}()
		return true
	} else {
		return false
	}
}
