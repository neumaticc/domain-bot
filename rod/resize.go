package util

import (
	"bytes"
	"image"
	"image/draw"
	"image/jpeg"
	_ "image/png"
)

// resizeImage resizes the imgBytes to the set width
func resizeImage(imgBytes []byte, width int) ([]byte, error) {
	// decode the image
	img, _, err := image.Decode(bytes.NewReader(imgBytes))
	if err != nil {
		return nil, err
	}

	// convert to *image.RGBA
	rgba := image.NewRGBA(img.Bounds())
	draw.Draw(rgba, rgba.Bounds(), img, image.Point{0, 0}, draw.Src)

	// create rect to crop
	rect := image.Rect(0, 0, width, rgba.Bounds().Dy())
	croppedImg := rgba.SubImage(rect)

	// encode from *image.Image to []byte
	var b bytes.Buffer
	err = jpeg.Encode(&b, croppedImg, nil)
	if err != nil {
		return nil, err
	}

	return b.Bytes(), nil
}
