package util

import (
	"context"
	"fmt"
	"os"
	"time"

	"github.com/go-rod/rod"
)

type Response struct {
	Image     string
	Domain    string
	Registrar string
	Price     string
}

// DoRod opens tld-list.com, searches the name, extracts its host/price/registrar, takes a screenshot, uploads it, and returns a Response{}
func DoRod(domain string) (error, Response) {
	fmt.Println("got 1")
	var browser *rod.Browser

	// attempt to use the remote instance in production or local instance if unspecified
	// ** remote preferred in docker - rod gave me issues in a container
	if os.Getenv("ROD_URL") != "" {
		browser = rod.New().ControlURL(os.Getenv("ROD_URL")).MustConnect()
	} else {
		browser = rod.New().MustConnect()
	}

	defer browser.MustClose()
	_, cancel := context.WithTimeout(context.Background(), 2*time.Minute) // 2min timeout
	defer cancel()

	page := browser.MustPage("https://tld-list.com")
	if err := page.WaitLoad(); err != nil {
		fmt.Println("Error waiting for page load:", err)
		return err, Response{}
	}

	// extra time just in case
	time.Sleep(2 * time.Second)
	page.MustElement("#search").MustInput(domain)

	// wait for query to fully load
	// ** i could probably do it with detecting the loading indicator and such but i'm too lazy
	time.Sleep(20 * time.Second)

	// selectors (if they change, can be acquired through DevTools Copy>CSS Selector)
	host := page.MustElement("#DataTables_Table_0 > tbody > tr:nth-child(1) > td.no-border-left.name-col.dtfc-fixed-left > a").MustText()
	registrar := page.MustElement("span[itemprop=name]").MustText()
	price := page.MustElement("tr.odd:nth-child(1) > td:nth-child(3) > div:nth-child(1) > div:nth-child(2) > div:nth-child(1) > span:nth-child(1) > span:nth-child(2)").MustText()

	// screenshot the row
	element := page.MustElement("tr.odd:nth-child(1)")
	screenshot := element.MustScreenshot()
	// resize so its only the cheapest registrar col
	resized, err := resizeImage(screenshot, 562)
	if err != nil {
		panic(err)
	}

	// os.WriteFile("test.png", screenshot, 0644)

	url, err := uploadImage(resized)
	if err != nil {
		panic(err)
	}

	return nil, Response{
		Image:     url,
		Domain:    host,
		Registrar: registrar,
		Price:     price,
	}
}
