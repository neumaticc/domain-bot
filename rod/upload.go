package util

import (
	"bytes"
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/google/uuid"
)

// uploadImage uploads the provided image to s3, returning a media url
func uploadImage(image []byte) (string, error) {
	sess, err := session.NewSession(&aws.Config{
		Region:   aws.String("auto"),
		Endpoint: aws.String(os.Getenv("S3_ENDPOINT")),
		Credentials: credentials.NewStaticCredentials(
			os.Getenv("S3_ACCESS_KEY"), os.Getenv("S3_SECRET_KEY"), "",
		),
	})
	if err != nil {
		return "", err
	}

	id := uuid.NewString()

	uploader := s3.New(sess)
	_, err = uploader.PutObject(&s3.PutObjectInput{
		Bucket: aws.String(os.Getenv("S3_BUCKET")),
		Key:    aws.String(fmt.Sprintf("%s/%s.png", os.Getenv("S3_FOLDER"), id)),
		Body:   bytes.NewReader(image),
	})

	return fmt.Sprintf("%s/%s/%s.png", os.Getenv("S3_PUBLIC_URL"), os.Getenv("S3_FOLDER"), id), err
}
