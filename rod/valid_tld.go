package util

import (
	"embed"
	"log"
	"regexp"
	"strings"
)

//go:embed tlds.txt
var content embed.FS

var validTLDs map[string]struct{}

func init() {
	// read the TLDs into memory
	fileData, err := content.ReadFile("tlds.txt")
	if err != nil {
		log.Fatal(err)
	}

	validTLDs = make(map[string]struct{})
	lines := strings.Split(string(fileData), "\n")
	for _, line := range lines {
		tld := strings.TrimSpace(line)
		if tld != "" {
			validTLDs[tld] = struct{}{}
		}
	}
}

// isValidTLD checks whether a provided TLD exists (does NOT check punycode)
func isValidTLD(tld string) bool {
	_, valid := validTLDs[strings.ToLower(tld)]
	return valid
}

// isHostnameValid ensures a hostname conforms to rfc1123/rfc1035 hostname specifications
func isHostnameValid(hostname string) bool {
	// block consecutive, starting, or ending hyphens/periods, only alphanumeric host part
	if strings.HasPrefix(hostname, "-") || strings.HasSuffix(hostname, "-") || strings.Contains(hostname, "--") {
		return false
	}

	parts := strings.Split(hostname, ".")
	for _, part := range parts {
		// Check each part for length and character set
		if len(part) < 1 || len(part) > 63 {
			return false
		}
		match, _ := regexp.MatchString(`^[a-zA-Z0-9-]+$`, part)
		if !match {
			return false
		}
	}

	return true
}

// DomainValid checks whether a provided domain is allowed under rfc1123/rfc1035 and has a TLD which actually exists
func DomainValid(domain string) bool {
	parts := strings.Split(domain, ".")
	tld := parts[len(parts)-1]

	//
	hostnameValid := isHostnameValid(domain)
	tldValid := isValidTLD(tld)

	return hostnameValid && tldValid
}
