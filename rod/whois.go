package util

import (
	"strings"

	"github.com/likexian/whois"
)

// Whois checks if the provided domain exists in the IANA WHOIS
func Whois(domain string) (bool, error) {
	result, err := whois.Whois(domain)
	if err != nil {
		return false, err
	}

	// check if WHOIS data has actual stuff
	if strings.Contains(result, "Registrar:") || strings.Contains(result, "Domain Name:") {
		// registered
		return true, nil
	} else {
		// not registered
		return false, nil
	}
}
